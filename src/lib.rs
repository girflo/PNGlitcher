#![deny(unsafe_code)]

#[macro_use]
extern crate bitflags;

use image;
pub mod chunk;
mod common;
mod encoder;
mod filter;
mod srgb;
pub mod text_metadata;
mod traits;
use std::fs::File;
use std::io::BufWriter;

pub use crate::{
    common::*,
    encoder::{Encoder, EncodingError, StreamWriter, Writer},
    filter::{AdaptiveFilterType, FilterType},
};

pub fn duplicate(
    input: &str,
    output: &str,
    force_ouput_overwrite: bool,
    filter: u8,
    occurence: usize,
    gap: usize,
) {
    let glitch_input = open_input(input);
    let title = define_output_name(output);
    files_presence(&input, &title, force_ouput_overwrite);
    let mut writer = open_output(&title, filter, &glitch_input);

    writer
        .duplicate(&glitch_input.buffer, occurence, gap)
        .unwrap();
}

pub fn duplicate_sample(
    input: &str,
    output: &str,
    force_ouput_overwrite: bool,
    occurence: usize,
    gap: usize,
) {
    let glitch_input = open_input(input);

    for n in 0..5 {
        let title = define_output_name_with_filter(output, filter_name(n));
        let mut writer = open_output(&title, n, &glitch_input);
        files_presence(&input, &title, force_ouput_overwrite);

        writer
            .duplicate(&glitch_input.buffer, occurence, gap)
            .unwrap();
    }
}

pub fn wrong_filter(input: &str, output: &str, force_ouput_overwrite: bool, filter: u8) {
    let glitch_input = open_input(input);
    let title = define_output_name(output);
    files_presence(&input, &title, force_ouput_overwrite);

    let mut writer = open_output(&title, 0, &glitch_input);
    writer.wrong_filter(&glitch_input.buffer, filter).unwrap();
}

pub fn wrong_filter_sample(input: &str, output: &str, force_ouput_overwrite: bool) {
    let glitch_input = open_input(input);

    for n in 0..5 {
        let title = define_output_name_with_filter(output, filter_name(n));
        let mut writer = open_output(&title, n, &glitch_input);
        files_presence(&input, &title, force_ouput_overwrite);

        writer.wrong_filter(&glitch_input.buffer, n).unwrap();
    }
}

pub fn variable_filter(input: &str, output: &str, force_ouput_overwrite: bool, random: bool) {
    let glitch_input = open_input(input);
    let title = define_output_name(output);
    files_presence(&input, &title, force_ouput_overwrite);

    let mut writer = open_output(&title, 0, &glitch_input);
    writer
        .variable_filter(&glitch_input.buffer, random)
        .unwrap();
}

pub fn replace(
    input: &str,
    output: &str,
    force_ouput_overwrite: bool,
    filter: u8,
    occurence: usize,
) {
    let glitch_input = open_input(input);
    let title = define_output_name(output);
    files_presence(&input, &title, force_ouput_overwrite);

    let mut writer = open_output(&title, filter, &glitch_input);

    writer.replace(&glitch_input.buffer, occurence).unwrap();
}

pub fn replace_sample(input: &str, output: &str, force_ouput_overwrite: bool, occurence: usize) {
    let glitch_input = open_input(input);

    for n in 0..5 {
        let title = define_output_name_with_filter(output, filter_name(n));
        let mut writer = open_output(&title, n, &glitch_input);
        files_presence(&input, &title, force_ouput_overwrite);

        writer.replace(&glitch_input.buffer, occurence).unwrap();
    }
}

fn define_output_name(output: &str) -> String {
    if output.ends_with(".png") || output.ends_with(".PNG") {
        output.to_string()
    } else {
        format!("{}.png", output).to_string()
    }
}

fn define_output_name_with_filter(output: &str, filter: &str) -> String {
    if output.ends_with(".png") {
        format!("{}_{}.png", output.trim_end_matches(".png"), filter)
    } else if output.ends_with(".PNG") {
        format!("{}_{}.png", output.trim_end_matches(".PNG"), filter)
    } else {
        format!("{}_{}.png", output, filter)
    }
}

fn open_input(input: &str) -> GlitchInput {
    let image = image::open(&input).unwrap();
    let imgx;
    let imgy;
    let mut alpha = false;
    let buffer;
    if image.color() == image::ColorType::Rgba8 {
        let img = image::open(&input).unwrap().to_rgba8();
        (imgx, imgy) = (img.dimensions().0, img.dimensions().1);
        alpha = true;
        buffer = img.into_raw();
    } else {
        let img = image::open(&input).unwrap().to_rgb8();
        (imgx, imgy) = (img.dimensions().0, img.dimensions().1);
        buffer = img.into_raw();
    }
    let glitch_input = GlitchInput {
        imgx,
        imgy,
        alpha,
        buffer,
    };
    glitch_input
}

fn open_output(title: &str, filter: u8, glitch_input: &GlitchInput) -> Writer<BufWriter<File>> {
    let file = std::fs::File::create(&title).unwrap();
    let w = std::io::BufWriter::new(file);
    let mut enc = Encoder::new(w, glitch_input.imgx, glitch_input.imgy);
    if glitch_input.alpha {
        enc.set_color(ColorType::Rgba);
    } else {
        enc.set_color(ColorType::Rgb);
    }
    enc.set_filter(FilterType::from_u8(filter).unwrap());
    enc.write_header().unwrap()
}

struct GlitchInput {
    imgx: u32,
    imgy: u32,
    alpha: bool,
    buffer: Vec<u8>,
}

fn filter_name(filter: u8) -> &'static str {
    let filters = ["none", "sub", "up", "avg", "paeth"];
    filters[usize::from(filter)]
}

fn files_presence(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let in_file = std::path::Path::new(in_img).exists();
    let out_file = std::path::Path::new(out_img).exists();
    if in_file == false {
        panic!("The input file provided doesn't exist")
    }
    if out_file == true && force_ouput_overwrite == false {
        panic!("The output file provided alredy exists")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;
    use std::path::Path;

    #[test]
    fn duplicate_works() {
        duplicate("test/test.png", "test/duplicate.png", true, 3, 1, 0);
        assert!(Path::new("test/duplicate.png").exists());
        fs::remove_file("test/duplicate.png").unwrap();
    }

    #[test]
    fn duplicate_sample_works() {
        duplicate_sample("test/test.png", "test/duplicate_sample", true, 1, 0);
        let files = vec!["avg", "none", "paeth", "sub", "up"];
        for x in files.iter() {
            let path = format!("test/duplicate_sample_{}.png", x);
            assert!(Path::new(&path).exists());
            fs::remove_file(path).unwrap();
        }
    }

    #[test]
    fn replace_works() {
        replace("test/test.png", "test/replace.png", true, 0, 1);
        assert!(Path::new("test/replace.png").exists());
        fs::remove_file("test/replace.png").unwrap();
    }

    #[test]
    fn replace_sample_works() {
        replace_sample("test/test.png", "test/replace_sample", true, 1);
        let files = vec!["avg", "none", "paeth", "sub", "up"];
        for x in files.iter() {
            let path = format!("test/replace_sample_{}.png", x);
            assert!(Path::new(&path).exists());
            fs::remove_file(path).unwrap();
        }
    }

    #[test]
    fn variable_filter_works() {
        variable_filter("test/test.png", "test/variable_filter.png", true, true);
        assert!(Path::new("test/variable_filter.png").exists());
        fs::remove_file("test/variable_filter.png").unwrap();
    }

    #[test]
    fn wrong_filter_works() {
        wrong_filter("test/test.png", "test/wrong_filter.png", true, 0);
        assert!(Path::new("test/wrong_filter.png").exists());
        fs::remove_file("test/wrong_filter.png").unwrap();
    }
}
