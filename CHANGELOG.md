# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.2] - 2024-09-18

### Added

- Unit tests

### Changed

- Update bitflags to version 2.6
- Update crc32fast to version 1.4.2
- Update miniz_oxide to version 0.8
- Update image to version 0.25.2
- Update rand to version 0.8.5

## [0.3.1] - 2023-04-19

### Added

- Support for transparency

## [0.3.0] - 2023-04-13 

### Added

- Overwrite flag

### Fixed

- Remove warning for variant never constructed

## [0.2.0] - 2022-11-02

### Added

- New glitch algorithm called variable_filter

### Changed

- Refactor the glitch functions

## [0.1.3] - 2022-09-13

### Fixed

- Prevent file name ending with two png extension when the given output ends with .png

## [0.1.2] - 2022-08-25

### Fixed

- Fix README example for replace algorithm

### Changed

- Update dependencies

## [0.1.1] - 2022-06-08

### Changed

- Samples now use `_` instead of `-` and the filter name instead of the filter number in output file name
